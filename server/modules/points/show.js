/**
 * Middleware to get single Point
 */
'use strict';

const Point = require('./point');

module.exports = (request, response) => {
    if (!request.params.id) {
        return response.status(422).json({
            error: 'required params missing'
        });
    }

    let query = {
        _id: request.params.id
    };

    return Point
        .findOne(query)
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};
