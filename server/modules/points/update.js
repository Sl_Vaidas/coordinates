/**
 * Helper middleware, used to update a Point.
 */
'use strict';

const Point = require('./point');

module.exports = (request, response) => {

    let query = {
        _id: request.params.id
    };

    return Point
        .findOne(query)
        .then(( /*doc*/ ) => Point.update(query, request.body))
        .then(( /*updated*/ ) => response.send())
        .catch(error => response.status(422).json(error));
};
