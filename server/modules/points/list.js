/**
 * Find list of points based on model param
 */
'use strict';

const Point = require('./point');

module.exports = (request, response) => {
    return Point
        .find({})
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};