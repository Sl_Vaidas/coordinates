/**
 * Middleware to create points based on request body object provided
 */
'use strict';

const Point = require('./point');
const Square = require('../squares/square');
const SquareCalcHelper = require('./squareCalcHelper');
const getAllSquares = require('./getAllSquares');

const squaresAreEqual = (square1, square2) => {
    let equal = true;
    square1.points.forEach(pt1 => {
        let equalPointIndex = square2.points.findIndex(pt2 => pt2._id.toString() == pt1._id.toString());
        if (equalPointIndex == -1) {
            equal = false;
        }
    });

    return equal;
}

//Returns squares from squaresArray1, that are not found in squareArray2
const nonIntersect = (squaresArray1, squaresArray2) => {
    let resultSquares = [];

    if (squaresArray2.length == 0) {
        resultSquares = squaresArray1;
    } else {
        squaresArray1.forEach(square1 => {
            let hasDuplicateInArray = false;
            squaresArray2.forEach(square2 => {
                if (squaresAreEqual(square1, square2)) {
                    hasDuplicateInArray = true;
                }
            });
            if (!hasDuplicateInArray) {
                resultSquares.push(square1);
            }
        });
    }

    return resultSquares;
}


module.exports = (newPoint, pointList, coordListId) => {
    let foundSquares = [];
    pointList.push(newPoint);
    let newPointList = pointList;
    return Square.find({
            _coordList: coordListId
        })
        .populate('points')
        .then(oldSqaureList => {
            let newSquareList = getAllSquares(newPointList);
            foundSquares = nonIntersect(newSquareList, oldSqaureList);
            return foundSquares.map(x => ({
                points: x.points,
                _coordList: coordListId
            }));
        });
}