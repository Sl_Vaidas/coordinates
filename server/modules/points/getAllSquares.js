/**
 * Middleware to create points based on request body object provided
 */
'use strict';

const SquareCalcHelper = require('./squareCalcHelper');

module.exports =  (points) => {
    let pointsList = [];
    let squares = [];
    let distances = [];

    for (let a = 0; a < points.length - 1; a++) {
        for (let b = a + 1; b < points.length; b++) {
            distances.push({
                dist: SquareCalcHelper.distSq(points[a], points[b]),
                p1: points[a],
                p2: points[b]
            });
        }
    }

    let foundSquares = [];
    let grouped = SquareCalcHelper.groupBy(distances, 'dist', 'value');
    grouped.forEach((element, i, arr) => {
        for (let a = 0; a < element.value.length - 1; a++) {
            for (let b = a + 1; b < element.value.length; b++) {
                if (SquareCalcHelper.isRectangle(element.value[a], element.value[b])) {
                    if (SquareCalcHelper.isSquare(element.value[a].p1, element.value[a].p2, element.value[b].p1, element.value[b].p2)) {
                        foundSquares.push({
                            points: [
                                (element.value[a].p1),
                                (element.value[a].p2),
                                (element.value[b].p1),
                                (element.value[b].p2),
                            ],
                        });
                    }
                }
            }
        }
    });

    return foundSquares;
}