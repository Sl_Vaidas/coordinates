/**
 * Point
 * Manage attributes, CRUD actions and queries
 */
'use strict';

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

let schema = new mongoose.Schema({
    x: Number,
    y: Number,
    _coordList: {type: mongoose.Schema.Types.ObjectId, ref:"Coordlist"}
}, {
    versionKey: false
});

schema.plugin(timestamps);
module.exports = mongoose.model('Point', schema);
