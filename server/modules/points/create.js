/**
 * Middleware to create points based on request body object provided
 */
'use strict';

const Point = require('./point');
const Square = require('../squares/square');
const getNewSquares = require('./getNewSquares');

module.exports = (request, response) => {
    let newPoint = new Point(request.body);
    let pointsList = [];


    return Point.find({
            _coordList: request.body._coordList
        }).lean()
        .then(data => {
            let isDuplicate = data.findIndex(point => point.x == newPoint.x && point.y == newPoint.y) != -1;
            if (isDuplicate) {
                throw {
                    isDuplicate: true
                }
            };
            this.pointsList = data;
            return data;
        })
        .then(data => new Point(request.body).save())
        .then(data => {
            this.newPoint = data;
            return getNewSquares(data, this.pointsList, request.body._coordList)
        })
        .then(newSquares => newSquares.map(listItem => new Square(listItem)))
        .then(data => Square.create(data))
        .then(data => data ? Square.find({
            '_id': {
                $in: data.map(x => x._id)
            }
        }).populate('points') : [])
        .then(data => ({
            squares: data,
            point: this.newPoint
        }))
        .then(data => response.json(data))
        .catch(error => {
            console.log(error);
            return response.status(422).json(error);
        });
};