/**
 * Manage points
 *  create
 *  update
 *  get single and list
 *  delete
 */
'use strict';

const router = require('express').Router();

const create = require('./create');
const pointsImport = require('./pointsImport');
const list = require('./list');
const show = require('./show');
const update = require('./update');
const remove = require('./remove');

const PATH = '/api/points';
const SINGLE = '/api/points/:id';

router
    .get(PATH, list)
    .post(PATH, create)
    .post(`${PATH}/import`, pointsImport)
    .get(SINGLE, show)
    .put(SINGLE, update)
    .delete(SINGLE, remove);

module.exports = router;