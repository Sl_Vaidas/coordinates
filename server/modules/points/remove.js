/**
 * Helper middleware, used to delete a document.
 */
'use strict';

const Point = require('./point');
const Square = require('../squares/square');

module.exports = (request, response) => {

    let pointQuery = {
        _id: request.params.id
    };
    
        let squareQuery = {
            points: pointQuery
        };

    

    let pointId = request.params.id;

    return Point
        .findOne(pointQuery)
        .then(data => {
            if (!data) {
                return response.status(404).json({
                    error: 'not found'
                });
            }
        })
        .then(()=>
            Promise.all([
                Point.remove(pointQuery),
                Square.remove(squareQuery)
            ])
        )
        .then(( /*removed*/ ) => response.send())
        .catch(error => response.status(422).json(error));
};
