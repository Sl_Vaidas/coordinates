const minValue = -5000;
const maxValue = 5000;
const clean = (array) => {
    return array.filter(point =>{ 
        let isCorrect = false;
        if(Number.isInteger(point.x) && Number.isInteger(point.y)){
            if(point.x <= maxValue && point.x >= minValue 
            && point.y <= maxValue && point.y >= minValue){
                isCorrect = true;
            }
        } 
        return isCorrect;
    });
}

const distSq = (p1, p2) => (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);

const uniquePoints = (total, current, array) => {
    total = total || [];
    if (total.findIndex(x => (x.x == current.x && x.y == current.y)) == -1) {
        total.push(current);
    }

    return total;
}

const hasDuplicates = (array) => {
    return array.reduce(uniquePoints, []).length < array.length;
}

const isSquare = (p1, p2, p3, p4) => {
    // If lengths if (p1, p2) and (p1, p3) are same, then
    // following conditions must met to form a square.
    // 1) Square of length of (p1, p4) is same as twice
    //    the square of (p1, p2)
    // 2) p4 is at same distance from p2 and p3
    let formsSquare = false;

    if (!hasDuplicates([p1, p2, p3, p4])) {
        let d2 = distSq(p1, p2); // from p1 to p2
        let d3 = distSq(p1, p3); // from p1 to p3
        let d4 = distSq(p1, p4); // from p1 to p4
        //-------------------------------------------------
        if (d2 == d3 && 2 * d2 == d4 && !formsSquare) {
            formsSquare = (distSq(p2, p4) == distSq(p3, p4) && distSq(p2, p4) == d2);
        }
        // The below two cases are similar to above case
        if (d3 == d4 && 2 * d3 == d2 && !formsSquare) {
            formsSquare = (distSq(p2, p3) == distSq(p2, p4) && distSq(p2, p3) == d3);
        }
        if (d2 == d4 && 2 * d2 == d3 && !formsSquare) {
            formsSquare = (distSq(p2, p3) == distSq(p3, p4) && distSq(p2, p3) == d2);
        }
    }

    return formsSquare;
}

const isRectangle = (a, b) => {
    let c1x = 0.0;
    let c2x = 0.0;
    let c1y = 0.0;
    let c2y = 0.0;

    c1x = (a.p1.x + a.p2.x) / 2;
    c1y = (a.p1.y + a.p2.y) / 2;

    c2x = (b.p1.x + b.p2.x) / 2;
    c2y = (b.p1.y + b.p2.y) / 2;

    return (c1x == c2x && c1y == c2y);
}

const groupBy = (array, col, value) => {
    let r = [],
        o = {};
    array.forEach(function (a) {
        if (!o[a[col]]) {
            o[a[col]] = {};
            o[a[col]][col] = a[col];
            o[a[col]][value] = [];
            r.push(o[a[col]]);
        }
        o[a[col]][value].push(a);
    });
    return r;
};


module.exports = {
    clean,
    distSq,
    uniquePoints,
    hasDuplicates,
    isSquare,
    groupBy,
    isRectangle,
}