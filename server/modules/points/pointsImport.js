/**
 * Middleware to create points based on request body object provided
 */
'use strict';

const Point = require('./point');
const Square = require('../squares/square');
const ClearCoordList = require('../coordlist/clear');
const getAllSquares = require('./getAllSquares');
const SquareCalcHelper = require('./squareCalcHelper');

module.exports = (request, response) => {
    let pointsList = request.body;
    pointsList = SquareCalcHelper.clean(pointsList);
    let hasDuplicates = SquareCalcHelper.hasDuplicates(pointsList);
    let hasNotNumbers = pointsList.length != request.body.length;
    let coordId = pointsList[0]._coordList || 0;
    pointsList = pointsList.reduce(SquareCalcHelper.uniquePoints, []);
    let foundSquares = getAllSquares(pointsList);

    _coordList: request.params.id
    return ClearCoordList({
            params: {
                id: coordId
            }, response
        }).then(() => Point.create(pointsList)).then(data => {
            pointsList = data;
            foundSquares.forEach(square => {
                square.points = square.points.map(p => {
                    return data.find(x => x.x == p.x && x.y == p.y)
                })
                square._coordList = coordId;
            })
        })
        .then(() => Square.create(foundSquares))
        .then(data => response.json({
                squares: data,
                points: pointsList,
                hasDuplicates: hasDuplicates,
                hasNotNumbers: hasNotNumbers
        }))
        .catch(error => console.log(error));
};