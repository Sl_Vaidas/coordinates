/**
 * Middleware to create coordList based on request body object provided
 */
'use strict';

const Coordlist = require('./coordlist');

module.exports = (request, response) => {
    //remove lists with same name then add new
    let newPoint = new Coordlist(request.body);
    return Coordlist.find({name: newPoint.name})
        .then(data =>  Coordlist.remove({name:newPoint.name})
        )
        .then(()=> newPoint.save())    
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};
