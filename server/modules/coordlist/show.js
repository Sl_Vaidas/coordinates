/**
 * Middleware to get single CoordList
 */
'use strict';

const Coordlist = require('./coordlist');
const Point = require('../points/point');
const Square = require('../squares/square');

module.exports = (request, response) => {
    if (!request.params.id) {
        return response.status(422).json({
            error: 'required params missing'
        });
    }

    let query = {
        _id: request.params.id
    };

    return Promise.all([
            Coordlist.findOne(query).lean(), 
            Point.find({_coordList:request.params.id}).lean(),
            Square.find({_coordList:request.params.id}).lean(),
        ])
        .then(data => Object.assign({}, data[0], {points: data[1], squares:data[2]}))
        //.then(data => {console.log(data.squares); return data;})
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};
