/**
 * Helper middleware, used to update a CoordList.
 */
'use strict';

const Coordlist = require('./coordlist');

module.exports = (request, response) => {

    let query = {
        _id: request.params.id
    };

    return Coordlist
        .findOne(query)
        .then(( /*doc*/ ) => Coordlist.update(query, request.body))
        .then( updated => response.send(updated))
        .catch(error => response.status(422).json(error));
};
