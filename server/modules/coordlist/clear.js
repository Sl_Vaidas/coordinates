/**
 * Helper middleware, used to update a CoordList.
 */
'use strict';

const Coordlist = require('./coordlist');
const Point = require('../points/point');
const Square = require('../squares/square');

module.exports = (request, response) => {

    let query = {
        _coordList: request.params.id
    };

    return Point.find(query)
        .then(data => {
            if (!data) {
                return response.status(404).json({
                    error: 'not found'
                });
            }
            return Point.remove(query);
        })
        .then(() => {
            return Square.find(query)
                .then(data => {
                    if (!data) {
                        return response.status(404).json({
                            error: 'not found'
                        });
                    }
                    return Square.remove(query);
                })
        })
        .then(( /*removed*/ ) => {if(response){ return response.send(); }})
        .catch(error => response.status(422).json(error));

};