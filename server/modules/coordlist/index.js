/**
 * Manage coordList
 *  create
 *  update
 *  get single and list
 *  delete
 */
'use strict';

const router = require('express').Router();

const create = require('./create');
const list = require('./list');
const show = require('./show');
const update = require('./update');
const remove = require('./remove');
const clear = require('./clear');

const PATH = '/api/coordlist';
const SINGLE = '/api/coordlist/:id';

router
    .delete(`/api/coordlist/clear/:id`, clear)
    .get(PATH, list)
    .post(PATH, create)
    .get(SINGLE, show)
    .put(SINGLE, update)
    .delete(SINGLE, remove);

module.exports = router;