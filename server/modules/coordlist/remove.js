/**
 * Helper middleware, used to delete a document.
 */
'use strict';

const Coordlist = require('./coordlist');

module.exports = (request, response) => {

    let query = {
        _id: request.params.id
    };

    return Coordlist
        .findOne(query)
        .then(data => {
            if (!data) {
                return response.status(404).json({
                    error: 'not found'
                });
            }
            return Coordlist.remove(query);
        })
        .then(( /*removed*/ ) => response.send())
        .catch(error => response.status(422).json(error));
};
