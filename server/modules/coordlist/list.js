/**
 * Find list of coordList based on model param
 */
'use strict';

const Coordlist = require('./coordlist');

module.exports = (request, response) => {
    return Coordlist
        .find({})
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};