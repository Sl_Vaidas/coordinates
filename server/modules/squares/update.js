/**
 * Helper middleware, used to update a Square.
 */
'use strict';

const Square = require('./square');

module.exports = (request, response) => {

    let query = {
        _id: request.params.id
    };

    return Square
        .findOne(query)
        .then(( /*doc*/ ) => Square.update(query, request.body))
        .then(( /*updated*/ ) => response.send())
        .catch(error => response.status(422).json(error));
};
