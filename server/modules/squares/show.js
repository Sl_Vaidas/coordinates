/**
 * Middleware to get single Square
 */
'use strict';

const Square = require('./square');

module.exports = (request, response) => {
    if (!request.params.id) {
        return response.status(422).json({
            error: 'required params missing'
        });
    }

    let query = {
        _id: request.params.id
    };

    return Square
        .findOne(query)
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};
