/**
 * Helper middleware, used to delete a document.
 */
'use strict';

const Square = require('./square');

module.exports = (request, response) => {

    let query = {
        _id: request.params.id
    };

    return Square
        .findOne(query)
        .then(data => {
            if (!data) {
                return response.status(404).json({
                    error: 'not found'
                });
            }
            return Square.remove(query);
        })
        .then(( /*removed*/ ) => response.send())
        .catch(error => response.status(422).json(error));
};
