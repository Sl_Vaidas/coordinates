/**
 * Manage squares
 *  create
 *  update
 *  get single and list
 *  delete
 */
'use strict';

const router = require('express').Router();

const create = require('./create');
const list = require('./list');
const show = require('./show');
const update = require('./update');
const remove = require('./remove');

const PATH = '/api/squares';
const SINGLE = '/api/squares/:id';

router
    .get(PATH, list)
    .post(PATH, create)
    .get(SINGLE, show)
    .put(SINGLE, update)
    .delete(SINGLE, remove);

module.exports = router;