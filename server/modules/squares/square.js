/**
 * Square
 * Manage attributes, CRUD actions and queries
 */
'use strict';

const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');

let schema = new mongoose.Schema({
    points: [{type: mongoose.Schema.Types.ObjectId, ref:"Point"}],
    _coordList: {type: mongoose.Schema.Types.ObjectId, ref:"Coordlist"}    
}, {
    versionKey: false
});

schema.plugin(timestamps);
module.exports = mongoose.model('Square', schema);
