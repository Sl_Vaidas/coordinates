/**
 * Find list of squares based on model param
 */
'use strict';

const Square = require('./square');

module.exports = (request, response) => {
    return Square
        .find({})
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};