/**
 * Middleware to create squares based on request body object provided
 */
'use strict';

const Square = require('./square');

module.exports = (request, response) => {
    return new Square(request.body)
        .save()
        .then(data => response.json(data))
        .catch(error => response.status(422).json(error));
};
