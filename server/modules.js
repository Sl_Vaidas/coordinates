'use strict';
 
const points = require('./modules/points');
const squares = require('./modules/squares');
const coordlist = require('./modules/coordlist');
 
module.exports = app => {
    app
        .use(points)
        .use(squares)
        .use(coordlist);
};
 