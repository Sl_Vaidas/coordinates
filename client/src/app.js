import {PLATFORM} from 'aurelia-pal';

export class App {
  configureRouter(config, router) {
    config.title = 'Coordinate system';
    config.map([
      { route: ['', 'list'], name: 'list', moduleId: PLATFORM.moduleName('./home'), nav: true, title: 'Main' }
    ])
    .mapUnknownRoutes('', 'not-found');

    this.router = router;
  }
}
