import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import {Point} from '../models/point';

@inject(HttpClient)
export class PointsGateway {

  constructor(httpClient) {
    this.httpClient = httpClient;
  }

  getAllPoints() {
    return this.httpClient.fetch('points/')
      .then(response => response.json())
      .then(dto => dto.map(Point.fromObject));
  }

  getAllByListId(id) {
    return this.httpClient.fetch(`coordlist/${id}`)
      .then(response => response.json());
  }

  updateList(id, data) {
    return this.httpClient.fetch(`coordlist/${id}`, { method: 'PUT', body: json(data) });
  }

  createPoint(point) {
    return this.httpClient.fetch('points/', { method: 'POST', body: json(point) });
  }

  importPoints(points) {
    return this.httpClient.fetch('points/import/', { method: 'POST', body: json(points) });
  }

  updatePoint(id, point) {
    return this.httpClient.fetch(`points/${id}`, { method: 'PUT', body: json(point) });
  }

  deletePoint(id) {
    return this.httpClient.fetch(`points/${id}`, { method: 'DELETE' });
  }

  clearList(id) {
    return this.httpClient.fetch(`coordlist/clear/${id}`, { method: 'DELETE' });
  }
}
