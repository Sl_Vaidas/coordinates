import {inlineView} from 'aurelia-framework';

@inlineView('<template><router-view></router-view></template>')
export class Points {
  configureRouter(config) {
    config.map([
      { route: '', name: 'points', moduleId: PLATFORM.moduleName('./components/list'), title: 'point.x' }
    ])
    .mapUnknownRoutes('', 'not-found');
  }
}
