import {ValidationRules} from 'aurelia-validation';

export class Point {

  static fromObject(src) {
    return Object.assign(new Point(), src);
  }

  constructor() {
    ValidationRules
      .ensure('x')
        .required()
        .satisfiesRule('integerRange', -5000, 5000)
      .ensure('y')
        .required()
        .satisfiesRule('integerRange', -5000, 5000)
      .on(this);
  }
}
