import {
  inject,
  NewInstance,
  DOM
} from 'aurelia-framework';
import {
  PointsGateway
} from '../services/gateway';
import {
  DialogService
} from 'aurelia-dialog';
import {
  Prompt
} from '../../resources/dialogs/prompt';
import {
  Point
} from '../models/point';
import {
  ValidationController
} from 'aurelia-validation';
import {
  Animator
} from 'aurelia-templating';
import {
  InputDialog
} from '../../resources/dialogs/input-dialog';
import {
  FileDialog
} from '../../resources/dialogs/file-dialog';
import {
  AlertDialog
} from '../../resources/dialogs/alert-dialog';
import * as download from 'downloadjs';

@inject(PointsGateway, DialogService, NewInstance.of(ValidationController), DOM.Element, Animator)
export class List {
  constructor(pointsGateway, dialogService, validationController, element, animator) {
    this.pointsGateway = pointsGateway;
    this.dialogService = dialogService;
    this.validationController = validationController;
    this.point = new Point();
    this.element = element;
    this.animator = animator;

    this.showSpinner = false;
    this.pointsList = [];
    this.coordListId = 0;
    this.pagination = {pointsCount: 5, squaresCount: 5};
  }

  tryClear = () => {
    this.dialogService.open({
      viewModel: Prompt,
      model: 'Do you really want to clear the list?',
      lock: false
    })
      .whenClosed(response => {
        if (!response.wasCancelled) {
          this.pointsGateway.clearList(this.coordListId)
            .then(data => {
              if (!data.ok) throw new Exception();
              else return data;
            })
            .then(data => {
              this.pointsList.points = [];
              this.pointsList.squares = [];
            })
            .catch(error => this.alertDialog('Something went wrong, point not deleted'));
        }
      });
  }

  alertDialog = (message) =>{
    this.dialogService.open({
      viewModel: AlertDialog,
      model: message,
      lock: false
    });
  }

  tryDelete = (_pointId) => {
    this.dialogService.open({
      viewModel: Prompt,
      model: 'Do you really want to delete the point?',
      lock: false
    })
      .whenClosed(response => {
        if (!response.wasCancelled) {
          this.pointsGateway.deletePoint(_pointId)
            .then(data => {
              if (!data.ok) throw new Exception();
              else return data;
            })
            .then(data => this.pointsList.points.filter(obj => obj._id !== _pointId))
            .then(data => this.pointsList.points = data)
            .then(data => this.pointsList.squares.filter(obj => obj.points.findIndex(x => x._id === _pointId) === -1))
            .then(data => this.pointsList.squares = data)
            .catch(error => this.alertDialog('Something went wrong, point not deleted'));
        }
      });
  }


  emphasizeErrors = () => {
    const errors = this.element.querySelectorAll('.validation-message');
    return this.animator.animate(Array.from(errors), 'blink');
  }

  tryAdd = () => {
    return this.validationController.validate().then(validation => {
      if (validation.valid) {
        return this.pointsGateway.createPoint(this.point)
          .then(data => {
            if (!data.ok) {
              throw new Exception();
            } else {
              return data.json();
            }
          })
          .then(data => {
            this.pointsList.points.push(data.point);
            data.squares = data.squares || [];
            data.squares.forEach(element => {
              this.pointsList.squares.push(element);
            }, this);
          })
          .catch(error => error.json().then(e =>
            this.alertDialog(e.isDuplicate ? 'Sorry, can not add duplicate points' : 'Something went wrong, point was not created')
          ));
      }
      this.emphasizeErrors();
      return;
    });
  }


  tryRename = () => {
    this.dialogService.open({
      viewModel: InputDialog,
      model: 'Please enter list name',
      lock: false
    })
      .whenClosed(response => {
        if (!response.wasCancelled) {
          this.pointsGateway.updateList(this.coordListId, {
            name: response.output
          })
            .then(data => data.json())
            .then(data => {
              if (data) {
                this.pointsList.name = response.output;
              } else {
                throw new Error();
              }
            })
            .catch(error => this.alertDialog('Something went wrong, list was not renamed'));
        }
      });

    return false;
  }

  tryDownload = () => {
    this.dialogService.open({
      viewModel: Prompt,
      model: 'Do you really want to download the list?',
      lock: false
    })
      .whenClosed(response => {
        if (!response.wasCancelled) {
          download(this.pointsList.points.map(x => [x.x, x.y].join(' ')).join('\n'), 'list.txt', 'application/text');
        }
      });
    return false;
  }


  importPts = (importData) => {
    let file;
    let fr;
    file = importData[0];
    fr = new FileReader();
    fr.onload = (e) => {
      let newArr = e.target.result;
      let lines = newArr.split('\n');
      lines.length = lines.length > 5000 ? 5000 : lines.length;
      lines = lines.map(x => {
        let t = x.trim().split(/\s+/);
        return {
          x: Number.parseInt(t[0]),
          y: Number.parseInt(t[1]),
          _coordList: this.coordListId
        };
      });

      this.pointsGateway.importPoints(lines)
        .then(data => data.json())
        .then(data => {
          this.pointsList.points = (data.points);
          data.squares = data.squares || [];
          this.pointsList.squares = data.squares;
          this.showSpinner = false;
          return data;
        })
        .then(data => {
          let message = '';
          if (data.hasDuplicates) {
            message += 'duplicate points';
          }
          if (data.hasNotNumbers) {
            if (message.length > 0) {
              message += ', ';
            }
            message += 'not numbes';
          }
          if (message.length > 0) {
            message = message.charAt(0).toUpperCase() + message.slice(1);
            message += '  were found and ignored';
            this.alertDialog(message);
          }
        })
        .catch(error => {
          this.showSpinner = false;
          this.alertDialog('Something went wrong, list was not imported');
        });
    };
    fr.readAsText(file);

    return false;
  }

  tryUpload = () => {
    this.dialogService.open({
      viewModel: FileDialog,
      model: 'Please enter list name',
      lock: false
    })
      .whenClosed(response => {
        if (!response.wasCancelled) {
          this.importPts(response.output);
          this.showSpinner = true;
        }
      });
  }

  activate = (params) => {
    this.point._coordList = params.id;
    this.coordListId = params.id;
    return this.pointsGateway.getAllByListId(params.id)
      .then(pointsList => {
        this.pointsList = pointsList;
        this.pointsList.squares.map(square => {
          square.points = square.points.map(pointId => this.pointsList.points.find(x => x._id === pointId));

          return square;
        });
      });
  }
}
