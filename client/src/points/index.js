import {HttpClient} from 'aurelia-fetch-client';
import {Router} from 'aurelia-router';
import {PointsGateway} from './services/gateway';
import environment from 'environment';

export function configure(config) {
  const router = config.container.get(Router);
  router.addRoute({route: '/list/:id', name: 'pointlist', moduleId: PLATFORM.moduleName('points/main')});
  const httpClient = config.container.invoke(HttpClient).configure(config => {
    config
      .useStandardConfiguration()
      .withBaseUrl(environment.apiUrl);
  });
  const gateway = new PointsGateway(httpClient);
  config.container.registerInstance(PointsGateway, gateway);
}

