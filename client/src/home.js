import {inject} from 'aurelia-framework';
import {HttpClient, json} from 'aurelia-fetch-client';
import environment from 'environment';
import {DialogService} from 'aurelia-dialog';
import {Prompt} from './resources/dialogs/prompt';
import {InputDialog} from './resources/dialogs/input-dialog';

@inject(HttpClient, DialogService)
export class Home {
  constructor(httpClient, dialogService) {
    this.httpClient = httpClient;
    this.dialogService = dialogService;
    this.coordlist = [];
  }

  getLists() {
    return this.httpClient.fetch(environment.coordListUrl)
      .then(response => response.json());
  }

  deleteList(id) {
    return this.httpClient.fetch(`${environment.coordListUrl}/${id}`, { method: 'DELETE' });
  }

  saveList(data) {
    return this.httpClient.fetch(environment.coordListUrl, {
      method: 'POST',
      body: json(data)
    }).then(response => response.json());
  }

  tryAdd() {
    this.dialogService.open({
      viewModel: InputDialog,
      model: 'Please enter list name',
      lock: false })
    .whenClosed(response => {
      if (!response.wasCancelled) {
        this.saveList({name: response.output})
        .then(newList => {
          if (newList) {
            this.coordlist = this.coordlist.filter(x=> x.name !== newList.name);
            this.coordlist.push(newList);
          } else {
            alert('Something went wrong, list not deleted');
          }
        });
      }
    });
  }

  tryDelete(_id) {
    this.dialogService.open({
      viewModel: Prompt,
      model: 'Do you really want to delete the list?',
      lock: false })
    .whenClosed(response => {
      if (!response.wasCancelled) {
        this.deleteList(_id)
        .then(data => {
          if (data.ok) {
            this.coordlist = this.coordlist.filter(function( obj ) {
              return obj._id !== _id;
            });
          } else {
            alert('Something went wrong, list not deleted');
          }
        });
      }
    });
  }

  activate() {
    return this.getLists().then(data => {this.coordlist = data.splice(0);});
  }
}

