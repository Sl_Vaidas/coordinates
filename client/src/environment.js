export default {
  debug: true,
  testing: true,
  apiUrl: 'http://localhost:9000/api/',
  pointsUrl: 'http://localhost:9000/api/points',
  coordListUrl: 'http://localhost:9000/api/coordList'
};
