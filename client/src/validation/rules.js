import {ValidationRules} from 'aurelia-validation';

ValidationRules.customRule(
  'date', 
  (value, obj) => value === null || value === undefined || value === '' || !isNaN(Date.parse(value)), 
  '${$displayName} must be a valid date.'
);

ValidationRules.customRule(
  'maxFileSize',
  (value, obj, maxSize) => !(value instanceof FileList)
    || value.length === 0
    || Array.from(value).every(file => file.size <= maxSize),
  '${$displayName} must be smaller than ${$config.maxSize} bytes.',
  maxSize => ({ maxSize })
);

const hasOneOfExtensions = (file, extensions) => {
  const fileName = file.name.toLowerCase();
  return extensions.some(ext => fileName.endsWith(ext));
}


ValidationRules.customRule(
      'integerRange',
      (value, obj, min, max) => {
        let num = Number.parseInt(value);
        return num === null || num === undefined || (Number.isInteger(num) && num >= min && num <= max);
      },
      "${$displayName} must be an integer between ${$config.min} and ${$config.max}.",
      (min, max) => ({ min, max })
    );
