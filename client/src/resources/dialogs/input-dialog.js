import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

@inject(DialogController)
export class InputDialog {
  constructor(controller) {
    this.controller = controller;
    this.answer = null;

    controller.settings.lock = false;
    controller.settings.centerHorizontalOnly = false;
  }

  activate(headerText) {
    this.headerText = headerText;
  }
}


