import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

@inject(DialogController)
export class FileDialog {
  constructor(controller) {
    this.controller = controller;
    this.answer = null;

    controller.settings.lock = false;
    controller.settings.centerHorizontalOnly = false;
  }

  import = (data) => {
    this.importPts(data);
  }

  activate(headerText) {
    this.headerText = headerText;
  }
}


