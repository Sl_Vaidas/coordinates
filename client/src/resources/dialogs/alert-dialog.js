import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

@inject(DialogController)
export class AlertDialog {
  constructor(controller) {
    this.controller = controller;

    controller.settings.lock = false;
    controller.settings.centerHorizontalOnly = false;
  }

  activate(question) {
    this.question = question;
  }
}


