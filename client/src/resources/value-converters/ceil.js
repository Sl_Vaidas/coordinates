export class CeilValueConverter {
  toView(value) {
    return Math.ceil(value);
  }
}
